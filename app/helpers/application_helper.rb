module ApplicationHelper
  def app_name
    Rails.application.config.site_name
  end

  def app_abbreviation
    Rails.application.config.site_abbreviation
  end

end
