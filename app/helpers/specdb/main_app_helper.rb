module Specdb
  module MainAppHelper
    def specdb_spectra_list(compound)
      render '/specdb/spectra_list', compound: compound
    end

    # Sort spectra into a hash, by SpecDB type
    def sorted_spectra(spectra)
      sorted_spectra = {}
      sorted_spectra[:gcms] = []
      sorted_spectra[:predicted_gcms] = []
      sorted_spectra[:msms] = []
      sorted_spectra[:predicted_msms] = []
      sorted_spectra[:eims] = []
      sorted_spectra[:nmr1d] = []
      sorted_spectra[:nmr2d] = []

      spectra.each do |spectrum|
        if spectrum.instance_of?(Specdb::CMs)
          if spectrum.spectrum_type =~ /^Predicted/
            sorted_spectra[:predicted_gcms] << spectrum
          else
            sorted_spectra[:gcms] << spectrum
          end
        elsif spectrum.instance_of?(Specdb::MsMs)
          if spectrum.spectrum_type =~ /^Predicted/
            sorted_spectra[:predicted_msms] << spectrum
          else
            sorted_spectra[:msms] << spectrum
          end
        elsif spectrum.instance_of?(Specdb::EiMs)
          sorted_spectra[:eims] << spectrum
        elsif spectrum.instance_of?(Specdb::NmrOneD)
          sorted_spectra[:nmr1d] << spectrum
        elsif spectrum.instance_of?(Specdb::NmrTwoD)
          sorted_spectra[:nmr2d] << spectrum
        end
      end

      sorted_spectra
    end

    # Render navigation list options for searching (usually from
    # main application under "Search tab").
    def specdb_search_links
      render '/specdb/navigation_search_links'
    end
  end
end
