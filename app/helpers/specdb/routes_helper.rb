module Specdb
  module RoutesHelper
    def spectrum_path(spectrum)
      path_method = spectrum.class.to_s.
        underscore.sub("specdb/", "specdb_").sub("_result","") + "_path"
      send path_method, spectrum
    end

    def specdb_c_ms_search_path(*args); specdb.c_ms_search_path(*args); end
    def specdb_c_ms_peak_assignments_path(*args); specdb.c_ms_peak_assignments_path(*args); end
    def specdb_c_ms_peaks_path(*args); specdb.c_ms_peaks_path(*args); end
    def specdb_c_ms_index_path(*args); specdb.c_ms_index_path(*args); end
    def specdb_c_ms_path(*args); specdb.c_ms_path(*args); end
  end
end
