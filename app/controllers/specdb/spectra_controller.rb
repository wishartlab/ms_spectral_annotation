module Specdb
  class SpectraController < Specdb::SpecdbController
    respond_to :html, :json
    before_filter :get_spectrum_name

    def index
      objects = 
        if params[:inchikey]
          @spectrum_class.where :inchi_key => params[:inchikey], 
            :page => params[:page], :per_page => 15
        else
          @spectrum_class.where :page => params[:page], :per_page => 15
        end

      instance_variable_set "@#{@spectrum_name}", objects
      respond_with objects
    end

    def show
      @spectrum = @spectrum_class.find(params[:id])
      instance_variable_set "@#{@spectrum_name}", @spectrum
    end
    

    # GET or POST ms_ms/search
    def search
      rescue_from_connection_errors do
        if params[:commit].present?
          @results = @spectrum_result_class.find(:all, :params => params)
        end
      end
    end

    private 

    def get_spectrum_name
      @spectrum_class = "Specdb::#{controller_name.classify}".constantize
      if @spectrum_class != Specdb::EiMs # For now there is no search for EiMs
        @spectrum_result_class = "#{@spectrum_class}Result".constantize
      end
      @spectrum_name  = controller_name.classify.underscore.sub(/specdb\//, "")
    end


  end
end
