module Specdb
  class CMsController < Specdb::SpectraController
    include ApplicationHelper
    def search
      rescue_from_connection_errors do
        #params[:mass_charge_tolerence] = 0.1
        if params[:commit].present?
          @results = @spectrum_result_class.find(:all, :params => params)
          @search_peaks = parse_peak_list(params[:peaks])
        end
      end
    end

    def load_candidate
      # Get the compound data
      compound_data = load_compound(params["id"], "CMs")
      
      respond_to do |format|
        format.json {render json: compound_data.to_json }
      end
    end
  end
end