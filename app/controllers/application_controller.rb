class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  helper Unearth::ExtractableHelper
  helper Unearth::FilterableHelper
  helper Wishart::Engine.helpers
  helper Moldbi::StructureResourcesHelper
  helper Moldbi::MarvinHelper
  helper Specdb::RoutesHelper
  helper Specdb::MainAppHelper
  # helper CiteThis::CitationHelper
  # helper TmicBanner::Engine.helpers
end
