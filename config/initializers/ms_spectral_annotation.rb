Rails.application.configure do
  config.version = 4.0
  config.contactor = 'ms_spectral_annotation'
  config.site_name = 'Ms Spectral Annotation'
  config.site_abbreviation = 'MsSA'
  config.url = "http://www.mssa.ca"
end
